apiVersion: v1
kind: ConfigMap
metadata:
  name: hss-configs
data:
  acl.conf: |
    # Configuration file for the peer whitelist extension.
    #
    # This extension is meant to allow connection from remote peers, without actively
    # maintaining this connection ourselves (as it would be the case by declaring the
    # peer in a ConnectPeer directive).
    # The format of this file is very simple. It contains a list of peer names
    # separated by spaces or newlines.
    #
    # The peer name must be a fqdn. We allow also a special "*" character as the
    # first label of the fqdn, to allow all fqdn with the same domain name.
    # Example: *.example.net will allow host1.example.net and host2.example.net
    #
    # At the beginning of a line, the following flags are allowed (case sensitive) -- either or both can appear:
    # ALLOW_OLD_TLS : we accept unprotected CER/CEA exchange with Inband-Security-Id = TLS
    # ALLOW_IPSEC   : we accept implicitly protected connection with with peer (Inband-Security-Id = IPSec)
    # It is specified for example as:
    # ALLOW_IPSEC vpn.example.net vpn2.example.net *.vpn.example.net

    ALLOW_OLD_TLS   *.localdomain *.test3gpp.net
  hss.conf: |
    # -------- Local ---------
    # The first parameter in this section is Identity, which will be used to
    # identify this peer in the Diameter network. The Diameter protocol mandates
    # that the Identity used is a valid FQDN for the peer. This parameter can be
    # omitted, in that case the framework will attempt to use system default value
    # (as returned by hostname --fqdn).
    Identity = "hss.olm.svc.cluster.local";

    # In Diameter, all peers also belong to a Realm. If the realm is not specified,
    # the framework uses the part of the Identity after the first dot.
    Realm = "olm.svc.cluster.local";


    # This parameter is mandatory, even if it is possible to disable TLS for peers
    # connections. A valid certificate for this Diameter Identity is expected.
    TLS_Cred = "./conf/hss.cert.pem", "./conf/hss.key.pem";
    TLS_CA = "./conf/cacert.pem";


    # Disable use of TCP protocol (only listen and connect in SCTP)
    # Default : TCP enabled
    No_SCTP;


    # This option is ignored if freeDiameter is compiled with DISABLE_SCTP option.
    # Prefer TCP instead of SCTP for establishing new connections.
    # This setting may be overwritten per peer in peer configuration blocs.
    # Default : SCTP is attempted first.
    Prefer_TCP;


    # Disable use of IPv6 addresses (only IP)
    # Default : IPv6 enabled
    No_IPv6;


    # Overwrite the number of SCTP streams. This value should be kept low,
    # especially if you are using TLS over SCTP, because it consumes a lot of
    # resources in that case. See tickets 19 and 27 for some additional details on
    # this.
    # Limit the number of SCTP streams
    SCTP_streams = 3;


    # By default, freeDiameter acts as a Diameter Relay Agent by forwarding all
    # messages it cannot handle locally. This parameter disables this behavior.
    NoRelay;


    # Use RFC3588 method for TLS protection, where TLS is negociated after CER/CEA exchange is completed
    # on the unsecure connection. The alternative is RFC6733 mechanism, where TLS protects also the
    # CER/CEA exchange on a dedicated secure port.
    # This parameter only affects outgoing connections.
    # The setting can be also defined per-peer (see Peers configuration section).
    # Default: use RFC6733 method with separate port for TLS.

    #TLS_old_method;


    # Number of parallel threads that will handle incoming application messages.
    # This parameter may be deprecated later in favor of a dynamic number of threads
    # depending on the load.
    AppServThreads = 4;

    # Specify the addresses on which to bind the listening server. This must be
    # specified if the framework is unable to auto-detect these addresses, or if the
    # auto-detected values are incorrect. Note that the list of addresses is sent
    # in CER or CEA message, so one should pay attention to this parameter if some
    # adresses should be kept hidden.
    #ListenOn = "127.0.0.1";

    Port = 3868;
    SecPort = 5868;

    LoadExtension = "acl_wl.fdx" : "./conf/acl.conf";

    # -------- Extensions ---------

    #LoadExtension = "/usr/local/lib/freeDiameter/_sample.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/app_acct.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/app_diameap.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/app_radgw.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/app_redirect.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/app_sip.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dbg_interactive.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dbg_monitor.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dbg_msg_dumps.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dbg_msg_timings.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dbg_rt.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_3gpp2_avps.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_CreditControl.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_CxDx.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_Gx.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_NAS.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_Ro.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_Rx.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_S6mS6n.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_SGd.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_SLh.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_Sd.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_Sh.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_T4.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_T6aT6bT7.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_Tsp.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_dcca.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_dcca_3gpp.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_dcca_starent.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_draftload_avps.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_eap.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_etsi283034_avps.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_legacy_xml.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_mip6a.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_mip6i.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_nas_mipv6.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_nasreq.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc4004_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc4006bis_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc4072_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc4590_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc5447_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc5580_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc5777_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc5778_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc6734_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc6942_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc7155_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc7683_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_rfc7944_avps.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_sip.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29061_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29128_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29154_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29173_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29212_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29214_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29215_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29217_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29229_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29272_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29273_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29329_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29336_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29337_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29338_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29343_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29344_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29345_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29368_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts29468_avps.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_ts32299_avps.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/rt_busypeers.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/rt_default.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/rt_ereg.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/rt_ignore_dh.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/rt_load_balance.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/rt_randomize.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/rt_redirect.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/test_acct.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/test_app.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/test_hss.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/test_netemul.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/test_rt_any.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/test_sip.fdx";
    #LoadExtension = "/usr/local/lib/freeDiameter/dict_Rf.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_S6as6d.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_S6t.fdx";
    LoadExtension = "/usr/local/lib/freeDiameter/dict_S6c.fdx";


    # Load RFC4072 dictionary objects
    #LoadExtension = "dict_eap.fdx";

    # Load the Diameter EAP server extension (requires diameap.conf)
    #LoadExtension = "app_diameap.fdx" : "diameap.conf";

    # Load the Accounting Server extension (requires app_acct.conf)
    #LoadExtension = "app_acct.fdx" : "app_acct.conf";

    # -------- Peers ---------

    # The framework will actively attempt to establish and maintain a connection
    # with the peers listed here.
    # For only accepting incoming connections, see the acl_wl.fx extension.

    #ConnectPeer = "peer1.localdomain" { ConnectTo = "127.0.0.1"; };
  hss.json: |
    {"common": {
        "fdcfg": "conf/hss.conf",
        "originhost": "hss.olm.svc.cluster.local",
        "originrealm": "olm.svc.cluster.local",
        "prom_port": 9089
     },
     "hss": {
        "gtwhost": "*",
        "gtwport" : 9080,
        "restport" : 9081,
        "ossport" : 9082,
        "casssrv": "cassandra",
        "cassusr": "root",
        "casspwd": "root",
        "cassdb" : "vhss",
        "casscoreconnections" : 2,
        "cassmaxconnections" : 8,
        "cassioqueuesize" : 32768,
        "cassiothreads" : 2,
        "randv"  : true,
        "optkey" : "63bfa50ee6523365ff14c1f45f88737d",
        "reloadkey"  : false,
        "logsize": 20,
        "lognumber": 5,
        "logname": "logs/hss.log",
        "logqsize": 8192,
        "statlogsize": 20,
        "statlognumber": 5,
        "statlogname": "logs/hss_stat.log",
        "auditlogsize": 20,
        "auditlognumber": 5,
        "auditlogname": "logs/hss_audit.log",
        "statfreq": 2000,
        "numworkers": 4,
        "concurrent": 10,
        "ossfile": "conf/oss.json",
        "verifyroamingsubscribers": true
     }
    }
  oss.json: |
    {
      "option": {
        "id": "url",
        "type": "string"
      },
      "services": [
        {
          "id": "logger",
          "commands": [
            {
              "id": "describe_loggers"
            },
            {
              "id": "set_logger_level",
              "options": [
                {
                  "id": "name",
                  "type": "string"
                },
                {
                  "id": "level",
                  "type": "integer"
                }
              ]
            }
          ]
        },
        {
          "id": "stats",
          "commands": [
            {
              "id": "describe_stats_frequency"
            },
            {
              "id": "describe_stats_live"
            },
            {
              "id": "set_stats_frequency",
              "options": [
                {
                  "id": "frequency",
                  "type": "integer"
                }
              ]
            }
          ]
        }
      ]
    }
  Cass_Provisioning.sh: |
    #!/bin/bash
    echo "Executing Script"
    imsi=208014567891200
    msisdn=1122334455
    apn="apn1"
    opc="d4416644f6154936193433dd20a0ace0"
    sqn=96
    cassandra_ip="cassandra"
    mmeidentity="mme.olm.svc.cluster.local"
    no_of_users=1
    mmerealm="olm.svc.cluster.local"
    key="465b5ce8b199b49faa5f0a2ee238a6bc"
    isdn=19136246000
    id=1
    unreachability=1

    #mkdir -p /etc/hss/conf/
    #cp /bin/oai_db.cql /opt/c3po/hssdb/oai_db.cql

    until cqlsh $cassandra_ip 9042 --file /opt/c3po/hssdb/oai_db.cql;
      do echo "Provisioning HSSDB";
      sleep 2;
    done

    echo "hello"

    /opt/c3po/hssdb/data_provisioning_users.sh $imsi $msisdn $apn $key $no_of_users $cassandra_ip $opc $mmeidentity $mmerealm

    echo "This script has just run another script."

    /opt/c3po/hssdb/data_provisioning_mme.sh $id $isdn $mmeidentity $mmerealm $unreachability $cassandra_ip

    echo "done with Cassendra Provisioning"
    echo "started hhs process"

    cp /opt/c3po/hssdb/hss-run.sh /bin/hss-run.sh

    /bin/hss-run.sh
  data_provisioning_mme.sh: |
    #! /bin/bash
    # Copyright 2019-present Open Networking Foundation

    #Copyright (c) 2017 Sprint
    #
    # SPDX-License-Identifier: Apache-2.0

    ### Add an entry to the mme_identity table ######

    id=$1
    isdn=$2
    host=$3
    realm=$4
    uereachability=$5
    cassandra_ip=$6
    if [ "$*" == "" -o $# -ne 6 ] ; then
       echo -e "You must provide all of the arguments to the script\n"
       echo -e "$0 <id> <isdn> <host> <realm> <uereachability> <cassandra_ip>\n"
       exit
    fi

    cqlsh $cassandra_ip -e "INSERT INTO vhss.mmeidentity (idmmeidentity, mmeisdn, mmehost, mmerealm, ue_reachability) VALUES ($id, '$isdn', '$host', '$realm', $uereachability);"
    if [ $? -ne 0 ];then
       echo -e "Oops! Something went wrong adding to vhss.mmeidentity!\n"
       exit
    fi

    cqlsh $cassandra_ip -e "INSERT INTO vhss.mmeidentity_host (idmmeidentity, mmeisdn, mmehost, mmerealm, ue_reachability) VALUES ($id, '$isdn', '$host', '$realm', $uereachability);"
    if [ $? -ne 0 ];then
       echo -e "Oops! Something went wrong adding to vhss.mmeidentity_host!\n"
       exit
    fi

    echo -e "The mmeidentity provisioning is successfull\n"
  data_provisioning_users.sh: |
    #! /bin/bash

    #Copyright 2019-present Open Networking Foundation
    #Copyright (c) 2017 Sprint
    #
    #SPDX-License-Identifier: Apache-2.0

    ### Delete all rows in the test table ######

    #cqlsh 10.78.137.141 -e "TRUNCATE vhss.test_users ;"

    imsi=$1
    msisdn=$2
    apn=$3
    key=$4
    no_of_users=$5
    cassandra_ip=$6
    opc=$7
    mmeidentity=${8:-'mme.localdomain'}
    mmerealm=${9:-'localdomain'}
    if [ "$*" == "" -o $# -lt 7 ] ; then
       echo -e "You must provide all of the required arguments to the script\n"
       echo -e "$0 <imsi> <msisdn> <apn> <key> <no_of_users> <cassandra_ip> <opc> [<mmeidentity> <mmerealm>]\n"
       exit
    fi

    for (( i=1; i<=$no_of_users; i++ ))
    do
    	echo "IMSI=$imsi MSISDN=$msisdn"

    	cqlsh $cassandra_ip -e "INSERT INTO vhss.users_imsi (imsi, msisdn, access_restriction, key, opc, mmehost, mmeidentity_idmmeidentity, mmerealm, rand, sqn, subscription_data) VALUES ('$imsi', $msisdn, 41, '$key', '$opc', '$mmeidentity', 3, '$mmerealm', '2683b376d1056746de3b254012908e0e', 96, '{\"Subscription-Data\":{\"Access-Restriction-Data\":41,\"Subscriber-Status\":0,\"Network-Access-Mode\":2,\"Regional-Subscription-Zone-Code\":[\"0x0123\",\"0x4567\",\"0x89AB\",\"0xCDEF\",\"0x1234\",\"0x5678\",\"0x9ABC\",\"0xDEF0\",\"0x2345\",\"0x6789\"],\"MSISDN\":\"0x$msisdn\",\"AMBR\":{\"Max-Requested-Bandwidth-UL\":50000000,\"Max-Requested-Bandwidth-DL\":100000000},\"APN-Configuration-Profile\":{\"Context-Identifier\":0,\"All-APN-Configurations-Included-Indicator\":0,\"APN-Configuration\":{\"Context-Identifier\":0,\"PDN-Type\":0,\"Served-Party-IP-Address\":[\"0.0.0.0\",\"0.0.0.0\"],\"Service-Selection\":\"$apn\",\"EPS-Subscribed-QoS-Profile\":{\"QoS-Class-Identifier\":9,\"Allocation-Retention-Priority\":{\"Priority-Level\":15,\"Pre-emption-Capability\":0,\"Pre-emption-Vulnerability\":0}},\"AMBR\":{\"Max-Requested-Bandwidth-UL\":50000000,\"Max-Requested-Bandwidth-DL\":100000000},\"PDN-GW-Allocation-Type\":0,\"MIP6-Agent-Info\":{\"MIP-Home-Agent-Address\":[\"172.26.17.183\"]}}},\"Subscribed-Periodic-RAU-TAU-Timer\":0}}');"
    	if [ $? -ne 0 ];then
    	   echo -e "Oops! Something went wrong adding to vhss.users_imsi!\n"
    	   exit
    	fi

    	cqlsh $cassandra_ip -e "INSERT INTO vhss.msisdn_imsi (msisdn, imsi) VALUES ($msisdn, '$imsi');"
    	if [ $? -ne 0 ];then
    	   echo -e "Oops! Something went wrong adding to vhss.msisdn_imsi!\n"
    	   exit
    	fi

    	imsi=`expr $imsi + 1`;
    	msisdn=`expr $msisdn + 1`
    done

    echo -e "The provisioning is successful\n"
  hss-run.sh: |
    #!/bin/bash

    # Copyright 2019-present Open Networking Foundation
    #
    # SPDX-License-Identifier: LicenseRef-ONF-Member-Only-1.0

    set -ex

    CONF_DIR="/opt/c3po/hss/conf"
    LOGS_DIR="/opt/c3po/hss/logs"
    mkdir -p $LOGS_DIR

    #cp /etc/hss/conf/{acl.conf,hss.json,hss.conf,oss.json} $CONF_DIR
    cat $CONF_DIR/{hss.json,hss.conf}

    cd $CONF_DIR
    make_certs.sh hss olm.svc.cluster.local

    #namespace changes

    cd ..
    hss -j $CONF_DIR/hss.json
  make_certs.sh: |
    #! /bin/bash
    # Copyright 2019-present Open Networking Foundation

    #Copyright (c) 2017 Sprint
    #
    # SPDX-License-Identifier: Apache-2.0

    rm -rf demoCA
    mkdir demoCA
    echo 01 > demoCA/serial
    touch demoCA/index.txt

    HOST=$1
    DOMAIN=$2

    # CA self certificate
    openssl req  -new -batch -x509 -days 3650 -nodes -newkey rsa:1024 -out cacert.pem -keyout cakey.pem -subj /CN=ca.localdomain/C=FR/ST=BdR/L=Aix/O=fD/OU=Tests

    #
    openssl genrsa -out $HOST.key.pem 1024
    openssl req -new -batch -out $HOST.csr.pem -key $HOST.key.pem -subj /CN=$HOST.$DOMAIN/C=FR/ST=BdR/L=Aix/O=fD/OU=Tests
    openssl ca -cert cacert.pem -keyfile cakey.pem -in $HOST.csr.pem -out $HOST.cert.pem -outdir . -batch
  oai_db.cql: |
    CREATE KEYSPACE IF NOT EXISTS vhss WITH replication = {'class': 'NetworkTopologyStrategy', 'DC1': '1'};

    CREATE TABLE IF NOT EXISTS vhss.users_imsi (
        imsi text PRIMARY KEY,
        access_restriction int,
        idmmeidentity int,
        imei text,
        imei_sv text,
        key text,
        lipa_permissions text,
        mme_cap int,
        mmehost text,
        mmeidentity_idmmeidentity int,
        mmerealm text,
        ms_ps_status text,
        msisdn bigint,
        niddvalidity text,
        nir_dest_host text,
        nir_dest_realm text,
        opc text,
        pgw_id int,
        rand text,
        rfsp_index varint,
        sqn bigint,
        subscription_data text,
        supported_features text,
        ue_reachability varint,
        urrp_mme varint,
        user_identifier text,
        visited_plmnid text);

    CREATE TABLE IF NOT EXISTS vhss.msisdn_imsi (
    	msisdn bigint PRIMARY KEY,
    	imsi text
    );

    CREATE TABLE IF NOT EXISTS vhss.global_ids (
        table_name text PRIMARY KEY,
        id counter);

    CREATE TABLE IF NOT EXISTS vhss.mmeidentity_host (
        mmehost text PRIMARY KEY,
        idmmeidentity int,
        mmerealm text,
        ue_reachability varint,
        mmeisdn text);

    CREATE TABLE IF NOT EXISTS vhss.mmeidentity (
        idmmeidentity int PRIMARY KEY,
        mmehost text,
        mmerealm text,
        ue_reachability varint,
        mmeisdn text);

    CREATE TABLE IF NOT EXISTS vhss.events (
        scef_id text,
        scef_ref_id bigint,
        extid text,
        monitoring_event_configuration text,
        monitoring_type int,
        msisdn bigint,
        user_identifier text,
        primary key (scef_id, scef_ref_id)
    );

    CREATE TABLE IF NOT EXISTS vhss.events_msisdn (
        msisdn bigint,
        scef_id text,
        scef_ref_id bigint,
        primary key (msisdn, scef_id, scef_ref_id)
    );

    CREATE TABLE IF NOT EXISTS vhss.events_extid (
        extid text,
        scef_id text,
        scef_ref_id bigint,
        primary key (extid, scef_id, scef_ref_id)
    );

    CREATE TABLE IF NOT EXISTS vhss.extid (
        extid text primary key
    );

    CREATE TABLE IF NOT EXISTS vhss.extid_imsi (
        extid text,
        imsi text,
        primary key (extid, imsi)
    );

    CREATE TABLE IF NOT EXISTS vhss.extid_imsi_xref (
        imsi text,
        extid text,
        primary key (imsi, extid)
    );

